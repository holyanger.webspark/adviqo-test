import { ApiStatuses } from 'enums/api-statuses';

export type ResponseError = {
    error?: string,
    status: ApiStatuses,
}