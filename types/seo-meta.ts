export interface SeoMeta {
  title?: string,
  description?: string,
}