import { AdvisorsFilters } from 'store/advisors/types';
import { PaginationOptions } from 'types/pagination';

export enum AdvisorLanguages {
    ENGLISH = 'us',
    RUSSIAN = 'ru',
    UKRAINIAN = 'ua',
}

export type Advisor = {
    _id: string,
    name: string,
    avatar: string,
    isOnline: boolean,
    reviewsCount: number,
    rating: number,
    bio: string,
    language: AdvisorLanguages,
}

export type AdvisorListQuery = PaginationOptions & AdvisorsFilters;