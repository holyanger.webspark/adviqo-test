import classNames from 'classnames';
import Image from 'next/image';
import { Advisor } from 'types/advisor';
import { AdvisorOnlineStatus } from 'store/advisors/types';
import RatingStars from 'components/rating-stars';
import styles from './styles.module.scss';

interface AdvisorItemProps {
  data: Advisor,
}

const AdvisorItem = (props: AdvisorItemProps) => {
  const { data } = props;
  const { name, avatar, bio, isOnline, language, reviewsCount, rating } = data;
  const statusKey = isOnline ? AdvisorOnlineStatus.ONLINE : AdvisorOnlineStatus.OFFLINE;

  return (
    <div className={styles.wrapper}>
      <div className={styles.avatarWrapper}>
        <Image className={styles.avatar} src={avatar} alt={name} width={80} height={80} />
        <span className={classNames(styles.onlineStatus, styles[statusKey])} />
      </div>
      <div className={styles.infoWrapper}>
        <div className={styles.nameWrapper}>
          <h2 className={styles.name}>
            {name}
          </h2>
          <Image
            className={styles.languageFlag}
            src={`/img/flags/${language}.svg`}
            alt={language}
            width={16}
            height={16}
          />
        </div>
        <div className={styles.reviewsWrapper}>
          <RatingStars rateValue={rating} />
          <span className={styles.reviewsCount}>({reviewsCount})</span>
        </div>
        <p className={styles.bio}>{bio}</p>
      </div>
    </div>
  )
};

export default AdvisorItem;