import { ReactNode } from 'react';
import InfinityScroll from 'components/infinity-scroll';
import { useListAdvisorsQuery } from 'store/api';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { updateAdvisorsPage } from 'store/advisors';
import { getAdvisorFilters, getAdvisorsCurrentPage } from 'store/advisors/selectors';
import AdvisorItem from './item';
import styles from './styles.module.scss';

interface ListProps {
  children: ReactNode,
}

const List = (props: ListProps) => {
  return <div className={styles.advisorsList}>{props.children}</div>
}

const AdvisorsList = () => {
  const dispatch = useAppDispatch();
  const page = useAppSelector(getAdvisorsCurrentPage);
  const filters = useAppSelector(getAdvisorFilters);

  const updatePage = () => {
    dispatch(updateAdvisorsPage());
  }

  return (
    <InfinityScroll
      page={page}
      updatePage={updatePage}
      queryOptions={filters}
      useRtkSelector={useListAdvisorsQuery}
      ListComponent={List}
      ItemComponent={AdvisorItem}
    />
  )
};

export default AdvisorsList;