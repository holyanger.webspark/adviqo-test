import { useMemo } from 'react';
import classNames from 'classnames';
import { useTranslations } from 'use-intl';
import Input from 'components/fields/input';
import Select from 'components/fields/select';
import { SelectValue } from 'components/fields/select/types';
import { AdvisorLanguages } from 'types/advisor';
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { getAdvisorFilters } from 'store/advisors/selectors';
import { AdvisorOnlineStatus, AdvisorsFilterNames, AdvisorsFilterValue } from 'store/advisors/types';
import { setAdvisorsFilter } from 'store/advisors';
import styles from './styles.module.scss';

const AdvisorsFilters = () => {
  const intl = useTranslations();
  const dispatch = useAppDispatch();
  const filters = useAppSelector(getAdvisorFilters);

  const onFilterChange = (name: string, value: SelectValue) => {
    dispatch(setAdvisorsFilter({
      name: name as AdvisorsFilterNames,
      value: value as AdvisorsFilterValue
    }));
  };

  const advisorStatuses = useMemo(() => {
    return [
      { name: intl('advisorOnlineStatus'), value: AdvisorOnlineStatus.ONLINE },
      { name: intl('advisorOfflineStatus'), value: AdvisorOnlineStatus.OFFLINE },
    ];
  }, [intl]);

  const advisorLanguages = useMemo(() => {
    return (Object.keys(AdvisorLanguages) as Array<keyof typeof AdvisorLanguages>).map((langKey) => {
      return {
        name: intl(`${langKey}_advisorLang`),
        value: AdvisorLanguages[langKey],
      }
    })
  }, [intl]);

  const advisorSortOptions = useMemo(() => {
    return [
      { name: intl('advisorSortingMoreReviews'), value: 'reviewsCount_desc' },
      { name: intl('advisorSortingLessReviews'), value: 'reviewsCount_asc' },
      { name: intl('advisorSortingMoreRating'), value: 'rating_desc' },
      { name: intl('advisorSortingLessRating'), value: 'rating_asc' },
    ]
  }, [intl]);

  return (
    <div className={styles.filtersWrapper}>
      <div className={classNames(styles.searchWrapper, styles.filterWrapper)}>
        <Input
          name={AdvisorsFilterNames.SEARCH}
          value={filters[AdvisorsFilterNames.SEARCH]}
          fullWidth
          placeholder={intl("advisorSearchPlaceholder")}
          onChange={onFilterChange}
        />
      </div>
      <div className={styles.filterWrapper}>
        <Select
          name={AdvisorsFilterNames.ONLINE_STATUS}
          value={filters[AdvisorsFilterNames.ONLINE_STATUS]}
          fullWidth
          placeholder={intl("advisorStatusPlaceholder")}
          onChange={onFilterChange}
          options={advisorStatuses}
        />
      </div>
      <div className={styles.filterWrapper}>
        <Select
          name={AdvisorsFilterNames.LANGUAGE}
          value={filters[AdvisorsFilterNames.LANGUAGE]}
          fullWidth
          placeholder={intl("advisorLanguagePlaceholder")}
          onChange={onFilterChange}
          options={advisorLanguages}
        />
      </div>
      <div className={styles.filterWrapper}>
        <Select
          emptyOptionName={null}
          name={AdvisorsFilterNames.SORT_OPTION}
          value={filters[AdvisorsFilterNames.SORT_OPTION]}
          fullWidth
          placeholder={intl("advisorSortingPlaceholder")}
          onChange={onFilterChange}
          options={advisorSortOptions}
        />
      </div>
    </div>
  )
}

export default AdvisorsFilters;