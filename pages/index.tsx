import { GetStaticProps } from 'next';
import Layout from 'components/layout';
import AdvisorsFilters from 'features/advisors/filters';
import AdvisorsList from 'features/advisors/list';

const HomePage = () => {
  return (
    <Layout>
      <AdvisorsFilters />
      <AdvisorsList />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({locale}) => {
  return {
    props: {
      messages: (await import(`../locales/${locale}.json`)).default
    }
  };
}

export default HomePage;