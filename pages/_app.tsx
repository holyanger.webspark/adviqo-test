import { AppProps } from 'next/app';
import { NextIntlProvider } from 'next-intl';
import { Provider } from 'react-redux';
import { store } from 'store';
import 'scss/base.scss';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <NextIntlProvider messages={pageProps.messages}>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </NextIntlProvider>
  );
}