import type { NextApiRequest, NextApiResponse } from 'next';
import { getErrorMessage, getRandomNumberInRange, nextResponseError } from 'utils';
import { LOCALE_COOKIE_NAME } from 'utils/const';
import AdvisorServices from 'services/advisors';
import { Advisor, AdvisorLanguages } from 'types/advisor';
import { LangOptions } from 'enums/lang-options';
import { ApiStatuses } from 'enums/api-statuses';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Advisor[] | { error: string }>
) {
  const locale = req.cookies[LOCALE_COOKIE_NAME] || LangOptions.ENGLISH;
  const {
    search,
    language,
    status,
    sortOption,
    page = 1,
    limit = 20,
  } = req.query;

  const pageNumber = +page;
  const limitNumber = +limit;

  if (isNaN(pageNumber) || isNaN(limitNumber)) {
    return nextResponseError(res, {
      status: ApiStatuses.BAD_REQUEST,
      error: 'page & limit query parameters should be a number',
    });
  }

  const isError = !getRandomNumberInRange(0, 5);

  if (isError && pageNumber === 1) {
    return nextResponseError(res, { status: ApiStatuses.BAD_REQUEST });
  }

  try {
    const advisors = await AdvisorServices.getAdvisors({
      page: pageNumber,
      limit: limitNumber,
      search: search as string,
      language: language as AdvisorLanguages,
      status: status as string,
      sortOption: sortOption as string,
      locale,
    });

    return res.status(ApiStatuses.SUCCESS).json(advisors);
  } catch (err: unknown) {
    return nextResponseError(res, {
      status: ApiStatuses.SERVER_ERROR,
      error: getErrorMessage(err),
    });
  }
}
