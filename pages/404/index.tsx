import { GetStaticProps } from 'next';
import Image from 'next/image';
import { useTranslations } from 'use-intl';
import Layout from 'components/layout';
import img404 from 'public/img/404.svg';
import styles from './styles.module.scss';

const Page404 = () => {
  const intl = useTranslations();

  return (
    <Layout title={intl("app404")}>
      <div>
        <Image className={styles.img404} alt="404" src={img404} />
        <h1 className={styles.title}>{intl("notFoundPage")}</h1>
      </div>
    </Layout>
  )
};

export const getStaticProps: GetStaticProps = async ({locale}) => {
  return {
    props: {
      messages: (await import(`../../locales/${locale}.json`)).default
    }
  };
}

export default Page404;