const nextConfig = {
  swcMinify: true,
  i18n: {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
  },
  images: {
    domains: ['cloudflare-ipfs.com'],
    unoptimized: true,
  },
}

module.exports = nextConfig
