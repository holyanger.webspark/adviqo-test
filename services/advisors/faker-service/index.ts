import { getFaker, getRandomNumberInRange, paginate, sleep, sortObjectsList } from 'utils';
import { AdvisorServiceInterface } from 'services/advisors/types';
import { Advisor, AdvisorLanguages, AdvisorListQuery } from 'types/advisor';
import { AdvisorOnlineStatus } from 'store/advisors/types';
import { LangOptions } from 'enums/lang-options';
import { SortDirections } from 'enums/sort-directions';

class FakerAdvisorService implements AdvisorServiceInterface {
    private static instance: FakerAdvisorService;
    private advisors: Advisor[] = [];
    private readonly TOTAL_ADVISORS = 100;
    private locale: string = LangOptions.ENGLISH;

    constructor() {
        if (FakerAdvisorService.instance) return FakerAdvisorService.instance;
        return FakerAdvisorService.instance = this;
    }

    initAdvisors(locale: string) {
      const advisors: Advisor[] = [];

      for (let i = 0; i < this.TOTAL_ADVISORS; i++) {
          advisors.push(this.getRandomAdvisor(locale));
      }

      this.advisors = advisors;
    }

    setAdvisors(advisors: Advisor[]) {
        this.advisors = advisors;
    }

    getRandomAdvisor(locale: string): Advisor {
        const faker = getFaker(locale);
        const languages = Object.values(AdvisorLanguages);
        const langIndex = faker.datatype.number({ min: 0, max: languages.length - 1 });

        return {
            _id: faker.datatype.uuid(),
            avatar: faker.image.avatar(),
            bio: faker.lorem.paragraph(),
            name: faker.name.fullName(),
            reviewsCount: faker.datatype.number(),
            rating: faker.datatype.float({ min: 0, max: 5 }),
            isOnline: faker.datatype.boolean(),
            language: languages[langIndex],
        };
    }

    filterAdvisors(options: AdvisorListQuery): Advisor[] {
        const { page, limit, status, search, language, sortOption = '' } = options;
        let advisors = this.advisors.filter((advisor) => {
            const statusCondition = !status ? true : status === AdvisorOnlineStatus.ONLINE
              ? advisor.isOnline
              : !advisor.isOnline;

            const searchCondition = !search ? true : advisor.name.toLowerCase().includes(search.toLowerCase());

            const langCondition = !language ? true : advisor.language === language;

            return statusCondition && searchCondition && langCondition;
        });

        const [sortField, sortDirection] = sortOption.split('_');

        if (sortField && sortDirection) {
            advisors = sortObjectsList(advisors, sortField as keyof Advisor, sortDirection as SortDirections);
        }

        return paginate(advisors, limit, page);
    }

    async getAdvisors(options: AdvisorListQuery & { locale?: string }): Promise<Advisor[]> {
        const { locale = LangOptions.ENGLISH, ...advisorOptions } = options;

        if (!this.advisors.length || this.locale !== locale) {
            this.initAdvisors(locale);
            this.locale = locale;
        }

        await sleep(getRandomNumberInRange(0.5, 1));

        return this.filterAdvisors(advisorOptions);
    }
}

export default new FakerAdvisorService();