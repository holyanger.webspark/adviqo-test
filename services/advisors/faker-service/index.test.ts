import FakerAdvisorService from './index';
import { Advisor, AdvisorLanguages } from 'types/advisor';
import { AdvisorOnlineStatus, AdvisorsFilterNames } from 'store/advisors/types';

const mockAdvisors: Advisor[] = [
  {
    "_id": "4e67b4bd-a1a0-400d-bdb5-17c1ca8616b8",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/986.jpg",
    "bio": "Illo cum at eveniet quos vero nam molestiae beatae occaecati. Velit itaque nemo assumenda hic ad atque explicabo. Eius ex facilis praesentium assumenda nobis eius sapiente aliquid.",
    "name": "Kelley Kozey",
    "reviewsCount": 34962,
    "rating": 0.3,
    "isOnline": true,
    "language": AdvisorLanguages.ENGLISH
  },
  {
    "_id": "61931e1c-a70c-46dd-9c09-c8665028ba8c",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/451.jpg",
    "bio": "Ipsa et mollitia fuga. Officiis officia ipsa vel. Libero nesciunt voluptas ea. Accusamus sit magni accusamus. Quae deserunt magnam cum eaque nisi aliquid. Aperiam dolorum officiis et officiis praesentium omnis ullam officia possimus.",
    "name": "Sheryl McLaughlin",
    "reviewsCount": 15765,
    "rating": 4.84,
    "isOnline": true,
    "language": AdvisorLanguages.ENGLISH
  },
  {
    "_id": "7e66aa56-8ac2-4ded-a67b-0d5cdbbee3ea",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/515.jpg",
    "bio": "Ipsum facilis accusamus dicta quas. Atque ipsam sequi soluta. Iure magni voluptatum. Asperiores aliquam eius labore blanditiis ex rerum. Alias cum excepturi. Error neque vero iste sapiente officia eaque.",
    "name": "Mrs. Kirk Conroy",
    "reviewsCount": 72385,
    "rating": 3.71,
    "isOnline": false,
    "language": AdvisorLanguages.ENGLISH
  },
  {
    "_id": "87bc72c6-1317-48d6-8cc2-04942ef8c78d",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/187.jpg",
    "bio": "Fugiat laborum mollitia. Quibusdam tempora autem modi voluptates odio. Cupiditate tempora ea vitae exercitationem. A quo aperiam rerum dolorem quis inventore. In totam voluptatum laborum. Ullam accusamus incidunt quos nostrum blanditiis qui temporibus.",
    "name": "Daisy Hintz",
    "reviewsCount": 30260,
    "rating": 3.46,
    "isOnline": true,
    "language": AdvisorLanguages.UKRAINIAN
  },
  {
    "_id": "4bddd960-02be-4ed6-8dc6-c6b3ead6c0b9",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/127.jpg",
    "bio": "Saepe laboriosam incidunt reiciendis voluptates. Quis vel maxime commodi dicta recusandae. Sunt similique quas cumque exercitationem.",
    "name": "Eileen Denesik",
    "reviewsCount": 49103,
    "rating": 0.92,
    "isOnline": true,
    "language": AdvisorLanguages.UKRAINIAN
  },
  {
    "_id": "4ea24333-f0e3-405d-a540-d92749b0b50b",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1202.jpg",
    "bio": "Dicta saepe eius fugit. Neque neque saepe. Saepe similique nihil sequi hic vero est minima iure adipisci. Architecto consequuntur asperiores ab mollitia. Placeat libero molestias veritatis nostrum dolore voluptas inventore.",
    "name": "Gilbert Mann",
    "reviewsCount": 44772,
    "rating": 1.54,
    "isOnline": true,
    "language": AdvisorLanguages.ENGLISH
  },
  {
    "_id": "3e76e5a0-67eb-43e4-a587-fa69df655478",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1185.jpg",
    "bio": "Iusto debitis nulla architecto. Incidunt blanditiis nostrum pariatur autem. Ad perspiciatis accusantium veniam in nobis deserunt odio id.",
    "name": "Dorothy Quigley",
    "reviewsCount": 49678,
    "rating": 0.3,
    "isOnline": true,
    "language": AdvisorLanguages.UKRAINIAN
  },
  {
    "_id": "3c80a986-4216-4d39-958d-099cbc09659d",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/418.jpg",
    "bio": "Sunt consequatur placeat odio atque ut illo tempore eius. Aspernatur quasi perspiciatis temporibus reprehenderit nostrum nulla modi. Reiciendis voluptatem aspernatur nihil excepturi aperiam voluptas quidem eius. Asperiores rem pariatur in est.",
    "name": "Sidney Lemke",
    "reviewsCount": 88642,
    "rating": 3.22,
    "isOnline": false,
    "language": AdvisorLanguages.RUSSIAN
  },
  {
    "_id": "b1f6e656-46ff-472c-87a5-f597caded275",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/338.jpg",
    "bio": "Ab iure iure laborum delectus quibusdam officiis nulla. Deleniti nesciunt voluptatum ducimus sed quod deleniti est. Blanditiis error doloremque veniam sequi beatae assumenda. Quae ipsam assumenda optio. Optio consequatur quasi assumenda facere.",
    "name": "Miss Pamela Zemlak",
    "reviewsCount": 85474,
    "rating": 3.43,
    "isOnline": true,
    "language": AdvisorLanguages.RUSSIAN
  },
  {
    "_id": "c4da2050-f466-4e4e-8e2a-0d0b1a1fa8d5",
    "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/505.jpg",
    "bio": "Quae praesentium non molestiae nemo aliquid. Enim minima animi eaque alias et modi nemo. Adipisci eveniet porro. Quidem molestias rem deleniti ducimus repellendus incidunt recusandae. A iure est perferendis.",
    "name": "Gail Franey",
    "reviewsCount": 96003,
    "rating": 1.91,
    "isOnline": true,
    "language": AdvisorLanguages.RUSSIAN
  }
];

FakerAdvisorService.setAdvisors(mockAdvisors);

describe('test advisors pagination', () => {
  it('test pagination limit 10', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
    });

    expect(filteredAdvisors.length).toEqual(10);
  });

  it('test pagination limit negative (-5)', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: -5,
    });

    expect(filteredAdvisors.length).toEqual(5);
  });

  it('test pagination page negative (-1)', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: -1,
      limit: 10,
    });

    expect(filteredAdvisors.length).toEqual(0);
  });

  it('test pagination page number larger then data exist', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 100,
      limit: 10,
    });

    expect(filteredAdvisors.length).toEqual(0);
  });
});

describe('test advisors filters', () => {
  it('test isOnline advisor in advisors list', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
      [AdvisorsFilterNames.ONLINE_STATUS]: AdvisorOnlineStatus.ONLINE,
    });

    expect(filteredAdvisors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          isOnline: true,
        })
      ])
    );
  });

  it('test language advisor in advisors list', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
      [AdvisorsFilterNames.LANGUAGE]: AdvisorLanguages.ENGLISH,
    });

    expect(filteredAdvisors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          language: AdvisorLanguages.ENGLISH,
        })
      ])
    );
  });

  it('test search advisor in advisors list', async () => {
    const searchStringToTest = 'Dorothy';

    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
      [AdvisorsFilterNames.SEARCH]: searchStringToTest,
    });

    expect(filteredAdvisors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: expect.stringContaining(searchStringToTest),
        })
      ])
    );
  });
});

describe('test advisors sorting', () => {
  it('test rating sorting in advisors list', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
      [AdvisorsFilterNames.SORT_OPTION]: 'rating_desc',
    });

    expect(filteredAdvisors).toBeSortedBy('rating', {
      descending: true,
    });
  });

  it('test reviewsCount sorting in advisors list', async () => {
    const filteredAdvisors = await FakerAdvisorService.filterAdvisors({
      page: 1,
      limit: 10,
      [AdvisorsFilterNames.SORT_OPTION]: 'reviewsCount_asc',
    });

    expect(filteredAdvisors).toBeSortedBy('reviewsCount', {
      descending: false,
    });
  });
});