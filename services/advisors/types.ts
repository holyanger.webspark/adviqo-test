import { Advisor, AdvisorListQuery } from 'types/advisor';

export interface AdvisorServiceInterface {
    getAdvisors(options: AdvisorListQuery): Promise<Advisor[]>;
}