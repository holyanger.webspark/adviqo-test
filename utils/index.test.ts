import { paginate, sortObjectsList } from './index';
import { SortDirections } from 'enums/sort-directions';

describe('test utils', () => {
  describe('test paginate', () => {
    const paginateMockData = [1, 2, 3, 4];

    it('test paginate page 1 limit 2', () => {
      expect(paginate(paginateMockData, 2, 1)).toEqual([1, 2]);
    });

    it('test paginate page 0 limit 0', () => {
      expect(paginate(paginateMockData, 0, 0)).toEqual([]);
    });

    it('test paginate data options largest then data exist', () => {
      expect(paginate(paginateMockData, 4, 2)).toEqual([]);
    });
  });

  describe('sort objects list', () => {
    const mockObjectsListData = [{ value: 1 }, { value: 3 }, { value: 5 }, { value: 4 }];

    expect(sortObjectsList(mockObjectsListData, 'value', SortDirections.DESC)).toBeSortedBy('value', {
      descending: true,
    });
  });
});