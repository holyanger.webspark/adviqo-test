import { faker } from '@faker-js/faker';
import { NextApiResponse } from 'next';
import { SortDirections } from 'enums/sort-directions';
import { ResponseError } from 'types/response-error';

export const getFaker = (locale: string) => {
  faker.locale = locale;
  return faker;
};

export const paginate = <T>(data: T[], limit: number, page: number) => {
  return data.slice((page - 1) * limit, page * limit);
};

export const sleep = async (seconds = 1) => {
  return new Promise<void>(resolve => {
    setTimeout(resolve, seconds * 1000);
  })
};

export const getRandomNumberInRange = (min = 0, max = 1) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const sortObjectsList = <T>(list: T[], field: keyof T, direction: SortDirections) => {
  return [...list].sort(function (a, b) {
    const firstField = a[field];
    const secondFiled = b[field];

    if (typeof firstField === 'number' && typeof secondFiled === 'number') {
      if (direction === SortDirections.ASC) {
        return firstField - secondFiled;
      }

      return secondFiled - firstField;
    }

    if (typeof firstField === 'string' && typeof secondFiled === 'string') {
      if (direction === SortDirections.ASC) {
        return firstField.localeCompare(secondFiled);
      }

      return secondFiled.localeCompare(firstField);
    }

    return 0;
  });
};

export const nextResponseError = (res: NextApiResponse, errorOptions: ResponseError) => {
  const { status, error = 'Something went wrong' } = errorOptions;

  return res.status(status).json({ status, error });
};

export const getErrorMessage = (error: unknown) => {
  return error instanceof Error ? error.message : 'Something went wrong';
};