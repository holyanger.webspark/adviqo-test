import { useTranslations } from 'use-intl';
import styles from './styles.module.scss';

interface ErrorMessageProps {
  message?: string,
}

const ErrorMessage = (props: ErrorMessageProps) => {
  const intl = useTranslations();
  const { message = intl('errorRequestMessage') } = props;

  return <p className={styles.message}>{message}</p>;
};

export default ErrorMessage;