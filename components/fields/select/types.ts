export type SelectValue = string | number;

export type SelectOption = {
  value: SelectValue,
  name: string | number,
};