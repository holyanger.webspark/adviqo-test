import React from 'react';
import classNames from 'classnames';
import { SelectValue, SelectOption } from '../types';
import styles from './styles.module.scss';

interface SelectOptionProps {
  onClick: (value: SelectValue) => void,
  isSelected?: boolean,
  option: SelectOption,
}

const Option = (props: SelectOptionProps) => {
  const { onClick, isSelected, option } = props;
  const { name, value } = option;

  const handleClick = () => {
    onClick(value);
  }

  return (
    <div onClick={handleClick} className={classNames(styles.option, { [styles.selected]: isSelected })}>
      {name}
    </div>
  )
};

export default Option;