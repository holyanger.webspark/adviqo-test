import React, { useState, useEffect, useRef, useCallback } from 'react';
import { useTranslations } from 'use-intl';
import classNames from 'classnames';
import Option from './option';
import { SelectOption, SelectValue } from './types';
import styles from './styles.module.scss';

interface SelectProps {
  name?: string,
  value?: SelectValue,
  onChange: (name: string, value: SelectValue) => void,
  placeholder?: string,
  emptyOptionName?: string | null,
  options: SelectOption[];
  fullWidth?: boolean,
}

const Select = (props: SelectProps) => {
  const intl = useTranslations();

  const {
    name,
    value,
    onChange = () => {},
    placeholder = '',
    fullWidth,
    emptyOptionName = intl('selectAllOptionsText'),
    options = [],
  } = props;

  const [open, setOpen] = useState(false);
  const refSelect = useRef<HTMLDivElement>(null);

  const toggleOpen = () => {
    setOpen(!open);
  };

  const checkIfClickedOutside = useCallback((e: MouseEvent) => {
    if (!(open && refSelect.current && !refSelect.current.contains(e.target as Node))) {
      return;
    }

    setOpen(false);
  }, [open]);

  useEffect(() => {
    document.addEventListener('mousedown', checkIfClickedOutside);
    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [checkIfClickedOutside]);

  const handleChange = (optionValue: SelectValue) => {
    setOpen(false);
    onChange(name || '', optionValue);
  };

  const getCurrentNameValue = () => {
    if (!value?.toString()) {
      return placeholder || intl('defaultSelectPlaceholder');
    }

    const option = options.find(({ value: optionValue }) => optionValue === value);

    if (!option) {
      return '';
    }

    return option.name;
  };

  const selectOptions = emptyOptionName ? [{ name: emptyOptionName, value: '' }, ...options] : options;

  return (
    <div
      className={classNames(styles.wrapper, { [styles.isActive]: open, [styles.fullWidth]: !!fullWidth })}
      ref={refSelect}
    >
      <div className={styles.selectItem} onClick={toggleOpen}>
        <span className={styles.selectedTextItem}>
          {getCurrentNameValue()}
        </span>
        <div className={styles.selectIcon}>
          <svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1 1L7 7L13 1" stroke="currentColor" strokeWidth="1.5" />
          </svg>
        </div>
        <div className={styles.selectOptions}>
          {selectOptions.map((option) => {
            const { value: optionValue } = option;
            const isSelected = value === optionValue;

            return (
              <Option
                key={optionValue}
                option={option}
                onClick={handleChange}
                isSelected={isSelected}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Select;
