import React, { useRef, useEffect, useState, useCallback } from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';

interface InputProps {
  placeholder?: string,
  label?: string,
  name?: string,
  value?: string,
  delay?: number,
  onChange: (name: string, value: string) => void,
  fullWidth?: boolean,
}

const Input = (props: InputProps) => {
  const {
    placeholder,
    label,
    name = '',
    onChange = () => {},
    value = '',
    delay = 1000,
    fullWidth,
  } = props;

  const [localValue, setLocalValue] = useState(value);
  const timer = useRef<number | null>(null);

  const getValue = useCallback((val: string) => {
    return val.trim().length ? val : val.trim();
  }, []);

  useEffect(() => {
    return setLocalValue(getValue(value?.toString() || ''));
  }, [getValue, value])

  useEffect(() => {
    return () => {
      clearTimeout(timer.current || 0);
      timer.current = null;
    }
  }, []);

  const triggerChange = (name: string, value: string) => {
    onChange(name, value);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = getValue(e.target.value);
    setLocalValue(inputValue);

    if (delay) {
      const { current } = timer;
      if (current) {
        clearTimeout(timer.current || 0);
      }

      timer.current = window.setTimeout(() => {
        triggerChange(name, inputValue);
      }, delay);

      return;
    }

    triggerChange(name, inputValue);
  };

  return (
    <label className={classNames(styles.inputWrapper, { [styles.fullWidth]: !!fullWidth })}>
      {label && (
        <span className={styles.label}>
          {label}
        </span>
      )}
      <input
        className={styles.input}
        type="text"
        value={localValue}
        placeholder={placeholder}
        onInput={handleChange}
      />
    </label>
  )
};

export default Input;
