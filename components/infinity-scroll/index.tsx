import { ElementType, useEffect, useState, useRef, useCallback } from 'react';
import classNames from 'classnames';
import Loader from 'components/loader';
import NoDataMessage from 'components/no-data-message';
import ErrorMessage from 'components/error-message';
import styles from './styles.module.scss';

type QueryOptions = Record<string, any>;

interface InfiniteScrollProps {
  page?: number,
  queryOptions?: QueryOptions,
  limit?: number,
  useRtkSelector: any,
  updatePage: () => void,
  ListComponent: ElementType,
  ItemComponent: ElementType,
}

const InfiniteScroll = (props: InfiniteScrollProps) => {
  const {
    page = 1,
    limit = 20,
    queryOptions = {},
    useRtkSelector = () => ({ data: [] }),
    ListComponent,
    ItemComponent,
    updatePage,
  } = props;

  const [listData, setListData] = useState<any[]>([]);
  const [hasMore, setHasMore] = useState(true);
  const needUpdate = useRef<boolean>(false);

  const { data = [], isFetching, isLoading, error } = useRtkSelector({
    ...queryOptions,
    page,
    limit
  });

  useEffect(() => {
    needUpdate.current = false;
    setListData([]);
    setHasMore(true);
  }, [queryOptions, error]);
  
  useEffect(() => {
    if (isLoading || error) {
      return;
    }

    setListData(prevState => [...prevState, ...data]);
    needUpdate.current = true;
    setHasMore(!(data.length < limit));
  }, [data, isLoading, limit, error]);

  const loader = useRef(null);

  const handleObserver = useCallback((entities: IntersectionObserverEntry[]) => {
    if (isLoading) {
      return;
    }

    const [target] = entities;

    if (!target.isIntersecting || !needUpdate.current) {
      return;
    }

    updatePage();
  }, [isLoading]);

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: '10px',
      threshold: 0.8,
    };

    const observer = new IntersectionObserver(handleObserver, options);

    if (!loader.current) {
      return;
    }

    observer.observe(loader.current);
  }, [handleObserver]);

  return (
    <>
      <ListComponent>
        {listData.map((item) => <ItemComponent data={item} key={item._id} />)}
      </ListComponent>
      {!listData.length && !isFetching && !error && <NoDataMessage />}
      {!!error && <ErrorMessage />}
      <div ref={loader} className={classNames(styles.loader, { [styles.hiddenLoader]: !hasMore || error })}>
        <Loader/>
      </div>
    </>
  )
}

export default InfiniteScroll;
