import { useTranslations } from 'use-intl';
import Image from 'next/image';
import EmptyDataImage from 'public/img/empty-box.png';
import styles from './styles.module.scss';

interface NoDataMessageProps {
  message?: string,
}

const NoDataMessage = (props: NoDataMessageProps) => {
  const intl = useTranslations();
  const { message = intl('noDataByQuery') } = props;

  return (
    <div className={styles.messageWrapper}>
      <Image src={EmptyDataImage} width={256} height={256} alt="" />
      <p className={styles.message}>{message}</p>
    </div>
  );
};

export default NoDataMessage;