import classNames from 'classnames';
import styles from './styles.module.scss';

interface RatingStarsProps {
  rateValue: number,
  maxRateValue?: number,
}

const RatingStars = (props: RatingStarsProps) => {
  const { rateValue, maxRateValue = 5 } = props;
  const roundedRate = Math.round(rateValue);

  return (
    <div className={styles.rateWrapper}>
      {Array.from(Array(maxRateValue).keys()).map(key => {
        const starIndex = key + 1;

        return (
          <div className={classNames(styles.star, { [styles.active]: starIndex <= roundedRate })} key={key}>
            <svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
              <rect id="Rectangle_4" width="24" height="24" fill="none"/>
              <path id="Star" d="M10,15,4.122,18.09l1.123-6.545L.489,6.91l6.572-.955L10,0l2.939,5.955,6.572.955-4.755,4.635,1.123,6.545Z" transform="translate(2 3)" fill="currentColor" stroke="none" strokeMiterlimit="10" strokeWidth="1.5" />
            </svg>
          </div>
        );
      })}
    </div>
  );
};

export default RatingStars;