import { useMemo } from 'react';
import { useTranslations } from 'use-intl';
import { useRouter } from 'next/router';
import { LOCALE_COOKIE_NAME } from 'utils/const';
import Select from 'components/fields/select';
import { SelectValue } from 'components/fields/select/types';
import { LangOptions } from 'enums/lang-options';

const SwitchLang = () => {
  const intl = useTranslations();
  const router = useRouter();
  const { locale, asPath } = router;

  const onChangeLanguage = (_: string, lang: SelectValue) => {
    document.cookie = `${LOCALE_COOKIE_NAME}=${lang}; path=/`;
    window.location.href = `/${lang}${asPath}`;
  };

  const langOptions = useMemo(() => {
    return (Object.keys(LangOptions) as Array<keyof typeof LangOptions>).map((langKey) => {
      return {
        name: intl(`${langKey}_langName`),
        value: LangOptions[langKey],
      }
    });
  }, [intl]);

  return (
    <Select
      value={locale}
      emptyOptionName={null}
      onChange={onChangeLanguage}
      options={langOptions}
    />
  )
};

export default SwitchLang;