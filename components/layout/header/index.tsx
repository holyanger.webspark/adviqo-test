import React from 'react';
import Link from 'next/link'
import SwitchLang from './switch-lang';
import styles from './styles.module.scss';

const Header = () => {
  return (
    <header className={styles.header}>
      <div className="container">
        <div className={styles.headerContent}>
          <Link href="/">
            <h1 className={styles.logoText}>{process.env.NEXT_PUBLIC_APP_NAME}</h1>
          </Link>
          <SwitchLang />
        </div>
      </div>
    </header>
  )
};

export default Header;
