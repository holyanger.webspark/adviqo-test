import { useTranslations } from 'next-intl';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { SeoMeta } from 'types/seo-meta';

interface MetaProps extends SeoMeta {}

const Meta = (props: MetaProps) => {
  const { title, description } = props;
  const router = useRouter();

  const intl = useTranslations();
  const metaTitle = title || intl('appTitle');
  const metaDescription = description || intl('appDescription');
  const { locale: currentLocale } = router;
  const link = `${process.env.NEXT_PUBLIC_APP_URL}/${currentLocale}${router.asPath}`;

  const baseImagePath = '/img/website-image.jpg';

  return (
    <Head>
      <title>{metaTitle}</title>
      <meta name="description" content={metaDescription} />
      <meta name="keywords" content={intl("appKeyWords")} />

      <meta property="og:title" content={metaTitle} key="og-title" />
      <meta property="og:locale" content={currentLocale} key="og-locale" />
      <meta property="og:url" content={link} key="og-url" />
      <meta property="og:description" content={metaDescription} key="og-description" />
      <meta property="og:image" content={baseImagePath} key="og-image" />
      <meta property="og:site_name" content="Adviqo test app" key="og-name" />
      <meta property="og:type" content="website" key="og-type" />
      <meta property="og:image:alt" content={title} key="og-image-alt" />

      <meta name="twitter:card" content={baseImagePath} key="twitter-card" />
      <meta property="twitter:domain" content="adviqo-test.vercel.app" />
      <meta property="twitter:url" content={link} />
      <meta name="twitter:image" content={baseImagePath} key="twitter-image" />
      <meta name="twitter:image:alt" content={title} key="twitter-image-alt" />
      <meta name="twitter:title" content={title} key="twitter-title" />
      <meta name="twitter:description" content={metaDescription} key="twitter-description" />
    </Head>
  );
};

export default Meta;