import { ReactNode } from 'react';
import { SeoMeta } from 'types/seo-meta';
import Header from './header';
import Footer from './footer';
import Meta from './meta';

export interface LayoutProps extends SeoMeta {
  children: ReactNode,
}

const Layout = (props: LayoutProps) => {
  const { children, title = '', description = '' } = props;

  return (
    <>
      <Meta title={title} description={description} />
      <Header/>
      <main>
        <div className="container">
          {children}
        </div>
      </main>
      <Footer/>
    </>
  )
};

export default Layout;
