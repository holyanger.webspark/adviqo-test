import Link from 'next/link'
import Copyright from './copyright';
import styles from './styles.module.scss';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className="container">
        <div className={styles.footerContentWrapper}>
          <Link className={styles.logoText} href="/">
            {process.env.NEXT_PUBLIC_APP_NAME}
          </Link>
          <Copyright />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
