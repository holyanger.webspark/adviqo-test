import { useTranslations } from 'use-intl';
import styles from './styles.module.scss';

const START_YEAR = 2022;

const Copyright = () => {
  const intl = useTranslations();

  const currentYear = new Date().getFullYear();
  const outputYear =
    START_YEAR === currentYear ? currentYear : `${START_YEAR} - ${currentYear}`;

  return (
    <p className={styles.copyright}>&copy; {outputYear} <b>{intl("footerCopyrightApp")}</b></p>
  );
};

export default Copyright;
