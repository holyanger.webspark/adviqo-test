First, create `.env.local` file in root app folder, you can put variables from `.env.example`

Install modules and run the development server:

```bash
npm install
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

For run tests:

```bash
npm run test
```

For production deploy application to [https://vercel.com](https://vercel.com)