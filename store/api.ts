import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { AdvisorListQuery } from 'types/advisor';

const serializeObjectToQuery = (queryObject: Record<string, string | number>) => {
  const queryArr = [];
  for (const queryName in queryObject) {
    const queryValue = queryObject[queryName];

    if (!queryValue) {
      continue;
    }

    queryArr.push(`${encodeURIComponent(queryName)}=${encodeURIComponent(queryValue)}`);
  }

  return queryArr.join('&');
}

export const apiSlice = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: '/api/' }),
  endpoints: (builder) => ({
    listAdvisors: builder.query({
      query: (options: AdvisorListQuery) => {
        return `advisors?${serializeObjectToQuery(options)}`;
      },
      keepUnusedDataFor: 0,
    }),
  }),
})

export const { useListAdvisorsQuery } = apiSlice;