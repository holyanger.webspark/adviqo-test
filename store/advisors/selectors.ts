import { RootState } from 'store/index';

export const getAdvisorFilters = (state: RootState) => state.advisors.filters;
export const getAdvisorsCurrentPage = (state: RootState) => state.advisors.page;