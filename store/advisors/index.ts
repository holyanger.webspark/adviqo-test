import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AdvisorsState, SetAdvisorFilterPayload } from './types';

const initialState: AdvisorsState = {
  filters: {
    search: '',
    language: '',
    status: '',
    sortOption: '',
  },
  page: 1,
}

const advisorsSlice = createSlice({
  name: 'advisors',
  initialState: initialState,
  reducers: {
    setAdvisorsFilter: (state, action: PayloadAction<SetAdvisorFilterPayload>) => {
      const { name, value } = action.payload;
      state.filters[name] = value;
      state.page = 1;
    },
    updateAdvisorsPage: (state) => {
      state.page++;
    }
  }
})

export const { setAdvisorsFilter, updateAdvisorsPage } = advisorsSlice.actions;

export default advisorsSlice.reducer;