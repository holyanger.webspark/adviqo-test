import { AdvisorLanguages } from 'types/advisor';

export enum AdvisorOnlineStatus {
  ONLINE = 'online',
  OFFLINE = 'offline',
}

export enum AdvisorsFilterNames {
  SEARCH = 'search',
  LANGUAGE = 'language',
  ONLINE_STATUS = 'status',
  SORT_OPTION = 'sortOption',
}

export type AdvisorsFilterValue = AdvisorLanguages | string | AdvisorOnlineStatus;

export type SetAdvisorFilterPayload = {
  name: AdvisorsFilterNames,
  value: AdvisorsFilterValue;
}

export type AdvisorsFilters = Partial<Record<AdvisorsFilterNames, AdvisorsFilterValue>>;

export interface AdvisorsState {
  filters: AdvisorsFilters,
  page: number,
}